# EtherCAT Master Example



## Project Details

- [ ] Written in Structured Text and Ladder
- [ ] Codesys Version 3.5.17.30
- [ ] Target: CC100 751-9301 FW23


## Name
Simple Example to Demomstrate EtherCAT using the 750-354

## Description
Simple Example to Demomstrate EtherCAT using the 750-354 to show the possible update speeds using EtherCAT versus other comms such as Modbus

![Program](EtherCAT.PNG){width=500 height=300}

## Support
This program is for demonstration only and does not include support.  May contain bugs.  Use at your own risk.

## Authors and acknowledgment
Jeff Govek
12-22-2022

## License
Copyright (c) 2024, Jeff Govek, WAGO

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE
